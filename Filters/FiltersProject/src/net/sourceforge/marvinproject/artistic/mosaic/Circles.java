package net.sourceforge.marvinproject.artistic.mosaic;
import java.awt.Color;
import java.awt.Graphics;

import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.performance.MarvinPerformanceMeter;
import marvin.plugin.MarvinAbstractPluginImage;
import marvin.plugin.MarvinPluginImage;
import marvin.util.MarvinAttributes;
import marvin.util.MarvinPluginLoader;

public class Circles extends MarvinAbstractPluginImage
{
    private int circleWidth;
    private int shift;
    private int circlesDistance;

    MarvinPerformanceMeter performanceMeter;
    private MarvinAttributes attributes;

    public void load(){
        attributes = getAttributes();
        attributes.set("circleWidth", 6);
        attributes.set("shift", 0);
        attributes.set("circlesDistance", 0);
        performanceMeter = new MarvinPerformanceMeter();
    }

    public void show(){
        PolishMarvinFilterWindow l_filterWindow = new PolishMarvinFilterWindow("Razsteryzacja-okr�gi", 420,350, getImagePanel(), this);
        l_filterWindow.addLabel("lblWidth", "Rozmiar okr�gu:");
        l_filterWindow.addTextField("txtCircleWidth", "circleWidth", attributes);
        l_filterWindow.addPanelBelow();
        l_filterWindow.addLabel("lblShift", "Przesuni�cie lini:");
        l_filterWindow.addTextField("txtShift", "shift", attributes);
        l_filterWindow.addPanelBelow();
        l_filterWindow.addLabel("lblDistance", "Odleg�o�� okr�g�w:");
        l_filterWindow.addTextField("lblCirclesDistance", "circlesDistance", attributes);
        l_filterWindow.setVisible(true);
    }

    public void process
    (
        MarvinImage a_imageIn, 
        MarvinImage a_imageOut,
        MarvinAttributes a_attributesOut,
        MarvinImageMask a_mask, 
        boolean a_previewMode
    )
    {
        double l_intensity;

        circleWidth = (Integer)attributes.get("circleWidth");
        shift = (Integer)attributes.get("shift");
        circlesDistance = (Integer)attributes.get("circlesDistance");

        Graphics l_graphics = a_imageOut.getBufferedImage().getGraphics();

        // Gray
        MarvinPluginImage l_filter = MarvinPluginLoader.loadPluginImage("net.sourceforge.marvinproject.color.grayScale.jar");
        l_filter.process(a_imageIn, a_imageIn, a_attributesOut, a_mask, a_previewMode);
        
        performanceMeter.enableProgressBar("Halftone - Circles" , (a_imageIn.getHeight()/(circleWidth+circlesDistance))*(a_imageIn.getWidth()/(circleWidth+circlesDistance)));
        
        boolean[][] l_arrMask = a_mask.getMaskArray();
        
        int l_dif=0;
        for (int y = 0; y < a_imageIn.getHeight(); y+=circleWidth+circlesDistance) {
            for (int x = 0+l_dif; x < a_imageIn.getWidth(); x+=circleWidth+circlesDistance) {
                if(l_arrMask != null && !l_arrMask[x][y]){
                    continue;
                }
                l_intensity = getSquareIntensity(x,y,a_imageIn);
                l_intensity+=1.0/circleWidth;
                l_graphics.setColor(Color.white);
                l_graphics.fillRect(x,y,circleWidth+circlesDistance,circleWidth+circlesDistance);
                l_graphics.setColor(Color.black);
                l_graphics.fillArc((int)(x+(circleWidth-(l_intensity*circleWidth))/2), (int)(y+(circleWidth-(l_intensity*circleWidth))/2), (int)(l_intensity*(circleWidth)), (int)(l_intensity*(circleWidth)),1,360);
            }
            l_dif = (l_dif+shift)%circleWidth;
            performanceMeter.stepsFinished((a_imageIn.getWidth()/(circleWidth+circlesDistance)));
        }
        a_imageOut.updateColorArray();
        performanceMeter.finish();
    }

    private double getSquareIntensity(int a_x, int a_y, MarvinImage image){
        double l_totalValue=0;
        @SuppressWarnings("unused")
		double l_pixels=0;
        for(int y=0; y<circleWidth; y++){
            for(int x=0; x<circleWidth; x++)
            {
                if(a_x+x > 0 && a_x+x < image.getWidth() &&  a_y+y> 0 && a_y+y < image.getHeight()){
                    l_pixels++;
                    l_totalValue+= 255-(image.getRed(a_x+x,a_y+y));
                }
            }
        }
        return (l_totalValue/(circleWidth*circleWidth*255));
    }
}
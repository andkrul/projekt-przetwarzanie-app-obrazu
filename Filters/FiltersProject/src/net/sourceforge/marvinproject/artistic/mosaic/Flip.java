package net.sourceforge.marvinproject.artistic.mosaic;

import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinAbstractPluginImage;
import marvin.util.MarvinAttributes;


public class Flip extends MarvinAbstractPluginImage
{
    private final static String HORIZONTAL = "poziomo";
    private final static String VERTICAL = "pionowo";

    private MarvinAttributes    attributes;
    private boolean[][]         arrMask;
    

    public void load(){
        attributes = getAttributes();
        attributes.set("flip", "horizontal");
    }

    public void show(){
        PolishMarvinFilterWindow l_filterWindow = new PolishMarvinFilterWindow("Przewr�c", 400,350, getImagePanel(), this);
        l_filterWindow.addLabel("labelTipo", "Type:");
        l_filterWindow.addComboBox("combpFlip", "flip", new Object[]{HORIZONTAL, VERTICAL}, attributes);
        l_filterWindow.setVisible(true);
    }

    public void process
    (
        MarvinImage a_imageIn, 
        MarvinImage a_imageOut,
        MarvinAttributes a_attributesOut,
        MarvinImageMask a_mask, 
        boolean a_previewMode
    )
    {
        String l_operation = (String)attributes.get("flip");
        arrMask = a_mask.getMaskArray();
        
        if(l_operation.equals(HORIZONTAL)){
            flipHorizontal(a_imageIn, a_imageOut);
        }
        else{
            flipVertical(a_imageIn, a_imageOut);
        }
    }

    private void flipHorizontal(MarvinImage a_imageIn, MarvinImage a_imageOut){
        int r,g,b;
        for (int y = 0; y < a_imageIn.getHeight(); y++) {
            for (int x = 0; x < (a_imageIn.getWidth()/2)+1; x++) {  
                if(arrMask != null && !arrMask[x][y]){
                    continue;
                }
                //Get Y points and change the positions 
                r = a_imageIn.getRed(x, y);
                g = a_imageIn.getGreen(x, y);
                b = a_imageIn.getBlue(x, y);

                a_imageOut.setRGB(x,y,
                        a_imageIn.getRed((a_imageIn.getWidth()-1)-x, y),
                        a_imageIn.getGreen((a_imageIn.getWidth()-1)-x, y),
                        a_imageIn.getBlue((a_imageIn.getWidth()-1)-x, y)
                );

                a_imageOut.setRGB((a_imageIn.getWidth() - 1) - x, y, r, g, b);
            }
        }
    }

    private void flipVertical(MarvinImage a_imageIn, MarvinImage a_imageOut){
        int r,g,b;
        for (int x = 0; x < a_imageIn.getWidth(); x++) {
            for (int y = 0; y < (a_imageIn.getHeight()/2)+1; y++) {
                if(arrMask != null && arrMask[x][y]){
                    continue;
                }
            
                //Get X points and change the positions 
                r = a_imageIn.getRed(x, y);
                g = a_imageIn.getGreen(x, y);
                b = a_imageIn.getBlue(x, y);

                a_imageOut.setRGB(x,y,
                        a_imageIn.getRed(x, (a_imageIn.getHeight()-1)-y),
                        a_imageIn.getGreen(x, (a_imageIn.getHeight()-1)-y),
                        a_imageIn.getBlue(x, (a_imageIn.getHeight()-1)-y)
                );

                a_imageOut.setRGB(x, (a_imageIn.getHeight() - 1) - y, r, g, b);
            }
        }
    }
}
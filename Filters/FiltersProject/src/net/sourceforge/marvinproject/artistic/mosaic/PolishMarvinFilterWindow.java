package net.sourceforge.marvinproject.artistic.mosaic;




import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import marvin.gui.MarvinFilterWindow;
import marvin.gui.MarvinImagePanel;
import marvin.gui.MarvinPluginWindow;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinPluginImage;


public class PolishMarvinFilterWindow extends MarvinPluginWindow
{
/**
	 * 
	 */
	
	private static final long serialVersionUID = -2410075001844049588L;

protected JPanel    panelFixedComponents, panelImage;

protected JButton   buttonPreview, buttonApply, buttonReset;
protected JLabel    labelImage;

MarvinImagePanel    imagePanel;
MarvinImage         imageThumbnail;
MarvinImage         imageResetBuffer;
MarvinImage         imageOut;
MarvinPluginImage   plugin; 

// ActionHandler
protected ActionHandler actionHandler;

/**
 * Constructs a {@link MarvinFilterWindow}
 * @param a_strName Window name
 * @param a_width width
 * @param a_height height
 * @param a_image {@link MarvinImage}
 * @param a_filter {@link MarvinFilter}
 */
public PolishMarvinFilterWindow
(
    String a_strName, 
    int a_width, 
    int a_height,
    MarvinImagePanel a_imagePanel,
    MarvinPluginImage a_filter
)
{
    this(a_strName, a_width, a_height, 250, 250, a_imagePanel, a_filter);
}

/**
 * Constructs a {@link MarvinFilterWindow}
 * @param a_strName Window name
 * @param a_width width
 * @param a_height height
 * @param a_thumbnailWidth thumbnail with
 * @param a_thumbnailHeight thumbnail height
 * @param a_image {@link MarvinImage}
 * @param a_filter {@link MarvinFilter}
 */
public PolishMarvinFilterWindow
(
    String a_strName, 
    int a_width, 
    int a_height,
    int a_thumbnailWidth,
    int a_thumbnailHeight,
    MarvinImagePanel a_imagePanel,
    MarvinPluginImage a_plugin
)
{
    super(a_strName, a_width, a_height);
    imagePanel = a_imagePanel;
    plugin = a_plugin;
    //Buttons
    actionHandler = new ActionHandler();
    buttonPreview = new JButton("Podgl�d");
    buttonReset = new JButton("Resetuj");
    buttonApply = new JButton("Zasotsuj");
    
    buttonPreview.setMnemonic('P');
    buttonReset.setMnemonic('R');
    buttonApply.setMnemonic('A');

    buttonPreview.addActionListener(actionHandler);
    buttonReset.addActionListener(actionHandler);
    buttonApply.addActionListener(actionHandler);

    // Fixed Components
    panelFixedComponents = new JPanel();
    panelFixedComponents.setLayout(new FlowLayout());
    panelFixedComponents.add(buttonPreview);
    panelFixedComponents.add(buttonReset);
    panelFixedComponents.add(buttonApply);

    // Image Panel
    panelImage = new JPanel();
    panelImage.setLayout(new FlowLayout());

    // Image
    if(a_thumbnailWidth > 0 && a_thumbnailHeight > 0){
        imageThumbnail = new MarvinImage(imagePanel.getImage().getImage(a_thumbnailWidth, a_thumbnailHeight, MarvinImage.PROPORTIONAL));            
        imageResetBuffer = imageThumbnail.clone();
        labelImage = new JLabel(new ImageIcon(imageThumbnail.getBufferedImage()));
        panelImage.add(labelImage);
    }
    
    imageOut = new MarvinImage(imagePanel.getImage().getWidth(), imagePanel.getImage().getHeight());
    
    container.add(panelImage, BorderLayout.NORTH);
    container.add(panelCenter, BorderLayout.CENTER);
    container.add(panelFixedComponents, BorderLayout.SOUTH);
    centerWindow();

}

/**
 * Disables preview function.
 */
public void disablePreview(){
    panelFixedComponents.remove(buttonPreview);
    panelFixedComponents.remove(buttonReset);
    panelImage.remove(labelImage);
}

/**
 * Returns the reference to "Apply" button.
 * @return a reference to "Apply" button
 */
public JButton getApplyButton(){
    return buttonApply;
}

/**
 * Preview the plug-in application
 */
public void preview(){
    try{
        //marvinApplication.getPerformanceMeter().disable();
        imageThumbnail = imageResetBuffer.clone();
        MarvinImage l_imageOut = new MarvinImage(imageThumbnail.getWidth(), imageThumbnail.getHeight());
        plugin.process(imageThumbnail, l_imageOut, null, MarvinImageMask.NULL_MASK, true);
        l_imageOut.update();
        imageThumbnail = l_imageOut.clone();
        //marvinApplication.getPerformanceMeter().enable();
        
    }
    catch(Exception e){
        e.printStackTrace();
    }
    labelImage.setIcon(new ImageIcon(imageThumbnail.getBufferedImage()));
}

/**
 * Reset to the original state
 */

private void centerWindow(){
	 Dimension windowSize = getSize();
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    Point centerPoint = ge.getCenterPoint();
    System.out.println(windowSize);
	int dx = centerPoint.x - windowSize.width / 2;
   int dy = centerPoint.y - windowSize.height / 2;    
   setLocation(dx, dy);
	
}
public void reset(){
    imageThumbnail = new MarvinImage(imageResetBuffer.getNewImageInstance());
    labelImage.setIcon(new ImageIcon(imageThumbnail.getBufferedImage()));
}

/**
 * Apply the plug-in
 */
public void apply(){        
    dispose();
    plugin.process(imagePanel.getImage(), imageOut, null, MarvinImageMask.NULL_MASK, false);
    
    if(imagePanel.isHistoryEnabled()){
        imagePanel.getHistory().addEntry("name", imageOut, plugin.getAttributes());
    }
    imageOut.update();
    imagePanel.setImage(imageOut);
}

/**
 * Event handler class
 */
private class ActionHandler implements ActionListener
{
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == buttonApply){
            applyValues();
            apply();
        }           
        else if (e.getSource() == buttonReset){
            reset();    
        }
        else if(e.getSource() == buttonPreview){
            applyValues();
            preview();
        }
    }
}
}
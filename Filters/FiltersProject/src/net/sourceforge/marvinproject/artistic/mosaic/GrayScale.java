package net.sourceforge.marvinproject.artistic.mosaic;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.performance.MarvinPerformanceMeter;
import marvin.plugin.MarvinAbstractPluginImage;
import marvin.util.MarvinAttributes;

public class GrayScale extends MarvinAbstractPluginImage
{
    MarvinPerformanceMeter performanceMeter;
    MarvinAttributes attributes;
    public void load(){
        performanceMeter = new MarvinPerformanceMeter();
        attributes = getAttributes();
    }

    public void show(){
        PolishMarvinFilterWindow l_filterWindow = new PolishMarvinFilterWindow("Skala szaro��i", 400,350, getImagePanel(), this);
        l_filterWindow.setVisible(true);
    }

    
    public void process
    (
        MarvinImage a_imageIn, 
        MarvinImage a_imageOut,
        MarvinAttributes a_attributesOut,
        MarvinImageMask a_mask, 
        boolean a_previewMode
    )
    { 
        // Mask
        boolean[][] l_arrMask = a_mask.getMaskArray();
        
        performanceMeter.start("Gray");
        performanceMeter.startEvent("Gray");
        int r,g,b,corfinal;
        for (int x = 0; x < a_imageIn.getWidth(); x++) {
            for (int y = 0; y < a_imageIn.getHeight(); y++) {
                if(l_arrMask != null && !l_arrMask[x][y]){
                    continue;
                }
                //Red - 30% / Blue - 59% / Green - 11%
                r = a_imageIn.getRed(x, y);
                g = a_imageIn.getGreen(x, y);
                b = a_imageIn.getBlue(x, y);
                corfinal = (int)((r*0.3)+(b*0.59)+(g*0.11));
                a_imageOut.setRGB(x,y,corfinal,corfinal,corfinal);
                                
            }
            performanceMeter.stepsFinished(a_imageIn.getHeight());
        }
        performanceMeter.finishEvent();
        performanceMeter.finish();
    }
}

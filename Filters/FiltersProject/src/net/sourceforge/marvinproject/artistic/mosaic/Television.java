package net.sourceforge.marvinproject.artistic.mosaic;


import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinAbstractPluginImage;
import marvin.util.MarvinAttributes;


public class Television extends MarvinAbstractPluginImage
{
    public void load(){}

    public void show(){
    	PolishMarvinFilterWindow l_filterWindow = new PolishMarvinFilterWindow("Telewizyjny", 400,350, getImagePanel(), this);
        l_filterWindow.setVisible(true);
    }

    public void process
    (
        MarvinImage a_imageIn, 
        MarvinImage a_imageOut,
        MarvinAttributes a_attributesOut,
        MarvinImageMask a_mask, 
        boolean a_previewMode
    )
    {
        boolean[][] l_arrMask = a_mask.getMaskArray();
        
        int r,g,b;
        for (int x = 0; x < a_imageIn.getWidth(); x++) {            
            for (int y = 0; y < a_imageIn.getHeight(); y+=3) {
                if(l_arrMask != null && !l_arrMask[x][y]){
                    continue;
                }
                
                r=0;
                g=0;
                b=0;
                    
                for(int w=0; w<3; w++){
                    if(y+w < a_imageIn.getHeight() ){
                        r += (a_imageIn.getRed(x, y+w))/2;
                        g += (a_imageIn.getGreen(x, y+w))/2;
                        b += (a_imageIn.getBlue(x, y+w))/2;                     
                    }
                }
                r = getValidInterval(r);
                g = getValidInterval(g);
                b = getValidInterval(b);
                        
                for(int w=0; w<3; w++){
                    if(y+w < a_imageOut.getHeight()){
                        if(w == 0){
                            a_imageOut.setRGB(x,y+w,r,0,0);
                        }
                        else if(w ==1){
                            a_imageOut.setRGB(x,y+w,0,g,0);
                        }
                        else if(w==2){
                            a_imageOut.setRGB(x,y+w,0,0,b);
                        }
                    }
                }               
            }
        }
    }

    public int getValidInterval(int a_value){
        if(a_value < 0) return 0;
        if(a_value > 255) return 255;
        return a_value;
    }
}
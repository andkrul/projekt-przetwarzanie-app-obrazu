package net.sourceforge.marvinproject.artistic.mosaic;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinAbstractPluginImage;
import marvin.util.MarvinAttributes;
import marvin.util.MarvinErrorHandler;
import marvin.util.MarvinFileChooser;


public class DifferenceGray extends MarvinAbstractPluginImage{

    public void load(){}

    public void show(){
        PolishMarvinFilterWindow l_filterWindow = new PolishMarvinFilterWindow("R�znica", 400,350, getImagePanel(), this);
        l_filterWindow.setVisible(true);
    }

    public void process
    (
        MarvinImage a_imageIn, 
        MarvinImage a_imageOut,
        MarvinAttributes a_attributesOut,
        MarvinImageMask a_mask, 
        boolean a_previewMode
    )
    {
        //Selects the other image to apply th difference
        String file = null;
        BufferedImage buffImage2=null;
        
        try {
            //Open the file browser dialog
            file = MarvinFileChooser.select(null, true, MarvinFileChooser.OPEN_DIALOG);
        } catch (Exception e) {
            MarvinErrorHandler.handle(MarvinErrorHandler.TYPE.ERROR_FILE_CHOOSE, e);
            return;
        }
        
        if(file == null) return;

        //Loads the image to the memory and creates an MarvinImage
        try{
            buffImage2 =  ImageIO.read(new File(file));
        }catch (IOException ioe) {
            MarvinErrorHandler.handle(MarvinErrorHandler.TYPE.ERROR_FILE_OPEN, ioe);
            return;
        }

        MarvinImage image2 = new MarvinImage(buffImage2);       
        
        //Gets the minimum width and height
        int minX = Math.min(a_imageIn.getWidth(), image2.getWidth());
        int minY = Math.min(a_imageIn.getHeight(), image2.getHeight());
        
        for (int x = 0; x < minX; x++) {
            for (int y = 0; y < minY; y++) {
                //Calculate the difference
                
                //Gets the gray scale value
                int gray = (int)((a_imageIn.getRed(x, y)*0.3) + (a_imageIn.getGreen(x, y)*0.11) + (a_imageIn.getBlue(x, y)*0.59));
                int gray1 = (int)((image2.getRed(x, y)*0.3) + (image2.getGreen(x, y)*0.11) + (image2.getBlue(x, y)*0.59));
                
                //Makes the absolute difference
                int diff = Math.abs(gray - gray1);
                int v = (diff / 2);
                
                //Sets the value to the new image
                a_imageOut.setRGB(x, y, v, v, v);
            }
        }
    }
}
package net.sourceforge.marvinproject.artistic.mosaic;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import marvin.gui.MarvinPluginWindow;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinAbstractPluginImage;
import marvin.util.MarvinAttributes;

public class Sepia extends MarvinAbstractPluginImage implements ChangeListener, KeyListener{

    private MarvinAttributes attributes;
    private MarvinPluginWindow Tela;
    
    public void load() {
        attributes = getAttributes();
        attributes.set("txtValue", "20");
        attributes.set("intensity", 20);
    }

    public void process
    (
        MarvinImage a_imageIn, 
        MarvinImage a_imageOut,
        MarvinAttributes a_attributesOut,
        MarvinImageMask a_mask, 
        boolean a_previewMode
    )
    {
        int r, g, b, depth, corfinal;
        
        //Define a intensidade do filtro...
        depth = Integer.parseInt(attributes.get("intensity").toString());
        
        
        //performanceMeter.enableProgressBar("Filtro de Teste", ((height-2)*(width-2)));
        
        boolean[][] l_arrMask = a_mask.getMaskArray();
        
        for (int x = 0; x < a_imageIn.getWidth(); x++) {
            for (int y = 0; y < a_imageIn.getHeight(); y++) {
                if(l_arrMask != null && !l_arrMask[x][y]){
                    continue;
                }
                //Captura o RGB do ponto...
                r = a_imageIn.getRed(x, y);
                g = a_imageIn.getGreen(x, y);
                b = a_imageIn.getBlue(x, y);
                
                //Define a cor como a m�dia aritm�tica do pixel...
                corfinal = (r + g + b) / 3;
                r = g = b = corfinal;
                 
                r = truncate(r + (depth * 2));
                g = truncate(g + depth);
            
                //Define a nova cor do ponto...
                a_imageOut.setRGB(x, y, r, g, b);
            }
            //performanceMeter.incProgressBar(width-2);
        }
        //performanceMeter.finish();
    }

    /**
     * Sets the RGB between 0 and 255
     * @param a
     * @return
     */
    public int truncate(int a) {
        if      (a <   0) return 0;
        else if (a > 255) return 255;
        else              return a;
    }
    
    public void show() { 
        PolishMarvinFilterWindow l_filterWindow = new PolishMarvinFilterWindow("Sepia", 400,350, getImagePanel(), this);
        l_filterWindow.addLabel("lblIntensidade", "Intensywno��");
        l_filterWindow.addHorizontalSlider("hsIntensidade", "hsIntensidade", 0, 100, 20, attributes);
        l_filterWindow.addPanelBelow();
        l_filterWindow.addTextField("txtValue", "txtValue",attributes);
        
        Tela = l_filterWindow;
            
        JTextField txtValue = (JTextField)(l_filterWindow.getComponent("txtValue").getComponent());
        JSlider slider = (JSlider)(l_filterWindow.getComponent("hsIntensidade").getComponent());
        
        slider.addChangeListener(this);
        txtValue.addKeyListener(this);
        
        l_filterWindow.setVisible(true);
    }
    
    //Manipula as altera��es da Horizontal Bar
    //Handles the Horizontal Bar changes
    public void stateChanged(ChangeEvent e) {
        JSlider barra = (JSlider) (e.getSource());
        JTextField lbl = (JTextField)(Tela.getComponent("txtValue").getComponent());
        lbl.setText(""+barra.getValue());
    }

    public void keyPressed(KeyEvent e) {
        
    }

    public void keyReleased(KeyEvent e) {
        JTextField txtValor = (JTextField) (Tela.getComponent("txtValue").getComponent());
        JSlider barra = (JSlider) (Tela.getComponent("hsIntensidade").getComponent());
        try {
            barra.setValue(Integer.parseInt(txtValor.getText().trim()));    
        } catch (Exception exception) {
            barra.setValue(0);
            txtValor.setText("0");
        }
            
    }

    public void keyTyped(KeyEvent e) {
    
    }
    
}
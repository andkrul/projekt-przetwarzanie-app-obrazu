package net.sourceforge.marvinproject.artistic.mosaic;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinAbstractPluginImage;
import marvin.util.MarvinAttributes;

public class Pixelize extends MarvinAbstractPluginImage
{
    MarvinAttributes    attributes;
    private boolean[][] arrMask;
    
    public void load(){
        attributes = getAttributes();
        attributes.set("squareSide", 10);
    }

    public void show(){
        PolishMarvinFilterWindow l_filterWindow = new PolishMarvinFilterWindow("Rozpikseluj", 400,350, getImagePanel(), this);
        l_filterWindow.addLabel("lblSquareSide", "Wielko�� pixela");
        l_filterWindow.addTextField("txtSquareSide", "squareSide", attributes);
        l_filterWindow.setVisible(true);
    }

    
    public void process
    (
        MarvinImage a_imageIn, 
        MarvinImage a_imageOut,
        MarvinAttributes a_attributesOut,
        MarvinImageMask a_mask, 
        boolean a_previewMode
    )
    {
        int l_rgb;
        
        int squareSide = (Integer)attributes.get("squareSide");
        arrMask = a_mask.getMaskArray();
            
        for (int x = 0; x < a_imageIn.getWidth(); x+=squareSide) {
            for (int y = 0; y < a_imageIn.getHeight(); y+=squareSide) {             
                l_rgb = getPredominantRGB(a_imageIn, x,y,squareSide);
                fillRect(a_imageOut, x,y,squareSide, l_rgb);                    
            }
        }
    }
    
    private int getPredominantRGB(MarvinImage a_image, int a_x, int a_y, int a_squareSide){
        int l_red=-1;
        int l_green=-1;
        int l_blue=-1;
        
        for(int x=a_x; x<a_x+a_squareSide; x++){
            for(int y=a_y; y<a_y+a_squareSide; y++){
                if(x < a_image.getWidth() && y < a_image.getHeight()){
                    if(arrMask != null && !arrMask[x][y]){
                        continue;
                    }
                    
                    if(l_red == -1)     l_red = a_image.getRed(x,y);
                    else                l_red = (l_red+a_image.getRed(x,y))/2;
                    if(l_green == -1)   l_green = a_image.getGreen(x,y);
                    else                l_green = (l_green+a_image.getGreen(x,y))/2;
                    if(l_blue == -1)    l_blue = a_image.getBlue(x,y);
                    else                l_blue = (l_blue+a_image.getBlue(x,y))/2;   
                }               
            } 
        }
        return (255<<24)+(l_red<<16)+(l_green<<8)+l_blue;
    }
    
    private void fillRect(MarvinImage a_image, int a_x, int a_y, int a_squareSide, int a_rgb){
        for(int x=a_x; x<a_x+a_squareSide; x++){
            for(int y=a_y; y<a_y+a_squareSide; y++){
                if(x < a_image.getWidth() && y < a_image.getHeight()){
                    if(arrMask != null && !arrMask[x][y]){
                        continue;
                    }
                    
                    a_image.setRGB(x,y,a_rgb);
                }
            }
        }                   
    }
}

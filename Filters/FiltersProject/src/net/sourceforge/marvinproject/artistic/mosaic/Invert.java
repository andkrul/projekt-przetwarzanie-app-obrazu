package net.sourceforge.marvinproject.artistic.mosaic;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinAbstractPluginImage;
import marvin.util.MarvinAttributes;

public class Invert extends MarvinAbstractPluginImage
{
    public void load(){}

    public void show(){
        PolishMarvinFilterWindow l_filterWindow = new PolishMarvinFilterWindow("Negatyw", 400,350, getImagePanel(), this);
        l_filterWindow.setVisible(true);
    }

    public void process
    (
        MarvinImage a_imageIn, 
        MarvinImage a_imageOut,
        MarvinAttributes a_attributesOut,
        MarvinImageMask a_mask, 
        boolean a_previewMode
    )
    {
        boolean[][] l_arrMask = a_mask.getMaskArray();
        
        int r, g, b;
        for (int x = 0; x < a_imageIn.getWidth(); x++) {
            for (int y = 0; y < a_imageIn.getHeight(); y++) {
                if(l_arrMask != null && !l_arrMask[x][y]){
                    continue;
                }
                r = (255-(int)a_imageIn.getRed(x, y));
                g = (255-(int)a_imageIn.getGreen(x, y));
                b = (255-(int)a_imageIn.getBlue(x, y));

                a_imageOut.setRGB(x,y,r,g,b);
            }
        }
    }
}
package net.sourceforge.marvinproject.artistic.mosaic;
import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.plugin.MarvinAbstractPluginImage;
import marvin.util.MarvinAttributes;

public class Scale extends MarvinAbstractPluginImage{
    
    private int                 width;
    private int                 height;
    private int                 newWidth;
    private int                 newHeight;
    
    private MarvinAttributes    attributes;
    
    public void load(){
        attributes = getAttributes();
        newWidth = 0;
        newHeight = 0;
        attributes.set("newWidth", newWidth);
        attributes.set("newHeight", newHeight);
    }
    
    public void show(){
        PolishMarvinFilterWindow l_filterWindow = new PolishMarvinFilterWindow("Skaluj", 270,100, getImagePanel(), this);
        l_filterWindow.disablePreview();
        l_filterWindow.addLabel("lblWidth", "Szeroko��:");
        l_filterWindow.addTextField("txtWidth", "newWidth", attributes);
        l_filterWindow.addPanelBelow();
        l_filterWindow.addLabel("lblHeight", "Wysoko��:");
        l_filterWindow.addTextField("txtHeight", "newHeight", attributes);

        l_filterWindow.setVisible(true);
    }
        
    public void process
    (
        MarvinImage a_imageIn, 
        MarvinImage a_imageOut,
        MarvinAttributes a_attributesOut,
        MarvinImageMask a_mask, 
        boolean a_previewMode
    ){
        
        width = a_imageIn.getWidth();
        height = a_imageIn.getHeight();
        newWidth = (Integer)attributes.get("newWidth");
        newHeight = (Integer)attributes.get("newHeight");
        
        a_imageOut.setDimension(newWidth, newHeight);
        
        int x_ratio = (int)((width<<16)/newWidth) ;
        int y_ratio = (int)((height<<16)/newHeight) ;
        int x2, y2 ;
        for (int i=0;i<newHeight;i++) {
            for (int j=0;j<newWidth;j++) {
                x2 = ((j*x_ratio)>>16) ;
                y2 = ((i*y_ratio)>>16) ;
                a_imageOut.setRGB(j,i, a_imageIn.getRGB(x2,y2));
            }                
        }       
    }
}